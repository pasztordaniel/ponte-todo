# Todo Mintaalkalmazás

Az alkalmazás egyszerű todo elemek nyilvántartásáról szól. A felső nézetben ha hozzáadsz egy elemet, akkor az elvárt működés alapján mind a felső mind az alsó listában meg kell jelennie az elemnek. Az alsó lista reprezentálja a model változtatások figyelését, például szerverszinkronizáció esetén történő módosulás lekövetést.

## Jelenlegi állapot

Az alkalmazás félkész, ennek az alkalmazásnak a befejezése, majd kibővítése a cél.

### Hátralévő feladatok

- Nincs megírva a `TodoItem` controller.
	- A nézete adott az `index.html`-ben, és elérhető a `TemplateProvider`-en keresztül
	- Ki kell tennie a `Todo` elem állapotát a checkbox-ba (checked/nem checked)
	- Ki kell írni a `Todo` elem nevét a megfelelő helyre
	- A checkbox-al frissítenie kell a mögöttes `Todo` elemet
	- `Todo` elem változásaira figyelnie kell és frissíteni a nézetet amennyiben szükséges
	- **Memóriakezelésre figyelni kell, ha a nézet elpusztul, szüntesse meg a Todo elem figyelését**

## Fejlesztendő feladatok

- `TodoStore` tudja menteni és visszatölteni a `Todo` elemeket `localStorage` segítségével
	- Legyen automata a mentés, minden `Todo` módosulásra mentsen

- Lehessen módosítani egy már felvett `Todo` szövegét nézeten keresztül
- A listából való törléskor legyen megerősítés
	- Ne használj `confirm` vagy más legacy API-t! Az esztétikája, hogy lenyílik-e az elem vagy dialógus az rád van bízva.

- A lista tudjon szűrni `completed` állapot alapján
	- szűrések: Minden, Elvégzett, Hátralévő
	- Az elemek elrejtéséhez használhatsz CSS-t is

- Legyen lehetőség törölni minden `completed: true` `Todo` elemet
	- Legyen megerősítés a törlés előtt

## Futtatás

Az `index.html` fájlt meg kell nyitni egy ECMAScript 6-ot támogató böngészőben:
- ECMAScript modulok
- `<template>` elem támogatott

A controller-ek template-ekkel működnek, a template-ek megtalálhatóak az `index.html`-ben.
