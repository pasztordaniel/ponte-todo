async function boot(el) {
	const moduleId = el.getAttribute("data-app");
	const resolvedModuleId = new URL(moduleId, location.href).toString();
	const mod = await import(resolvedModuleId);
	mod.run(el);
}

async function main() {
	const bootElements = Array.from(document.querySelectorAll("[data-app]"));
	const bootProcesses = bootElements.map((el) => boot(el));
	await Promise.all(bootProcesses);
}

main();
