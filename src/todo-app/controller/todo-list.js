import { Todo } from "../model/todo.js";
import { ItemAddedEvent, ItemRemovedEvent, ItemUpdateEvent, ItemFilterEvent } from "../model/todo-store.js";
import { addEventListener } from "../util/event-listener.js";
import { EventEmitter } from "../util/event-emitter.js";
import { Confirm } from "../util/confirm.js";

export class TodoList {
	constructor(todoStore, templateProvider) {
		this._todoStore = todoStore;
		this._templateProvider = templateProvider;
		if(localStorage.getItem("todoFilter")){
			this.actualFilterType = parseInt(localStorage.getItem("todoFilter"));
		}else{
			this.actualFilterType = 0;
		}
		this._elements = templateProvider.create("todoList");
		this._disposables = [];
		this._todoDescriptors = [];
		this.setFilterButtons();
	}

	get element() {
		return this._elements.root;
	}

	attached() {
		this._disposables.push(addEventListener(this._elements.addTodoForm, "submit", (e) => {
			e.preventDefault();
			const text = this._elements.addTodoText.value;
			if (!text) return;
			this._elements.addTodoText.value = "";
			this._todoStore.addItem(new Todo({
				completed: false,
				name: text,
				visible: true
			}));

			this._todoStore.filterBy(this._todoStore.items,0);
			this.saveFilter(0);
			this.setFilterButtons();
		}));
		
		this._disposables.push(addEventListener(this._elements.all, "click", (e) => {
			this._todoStore.filterBy(this._todoStore.items,0);
			this.saveFilter(0);
			this.setFilterButtons();
		}));
		
		this._disposables.push(addEventListener(this._elements.completed, "click", (e) => {
			this._todoStore.filterBy(this._todoStore.items,1);
			this.saveFilter(1);
			this.setFilterButtons();
		}));
		
		this._disposables.push(addEventListener(this._elements.remaining, "click", (e) => {
			this._todoStore.filterBy(this._todoStore.items,2);
			this.saveFilter(2);
			this.setFilterButtons();
		}));
		
		this._disposables.push(addEventListener(this._elements.removeAllCompleted, "click", (e) => {
			let confirm = new Confirm(this._templateProvider);
			let message = "Do You Want To Remove All Completed Todo Element?";
			confirm.showMessage(message, () => {
				this._todoStore.items.forEach((item) => {
					if(item._completed){
						const index = this._todoDescriptors.findIndex((descriptor) => descriptor.item === item);
						if (index === -1) throw new Error(`Unknown todo item: "${item.name}"`);
						const controller = this._todoDescriptors[index].controller;
						controller._delegate.remove();
					}
				});
			});
		}));

		this._todoStore.items.forEach((item) => {
			this.addItem(item);
		});

		this._disposables.push(this._todoStore.changeEventListener.add((event) => {
			if (event instanceof ItemAddedEvent) {
				this.addItem(event.item);
			} else if (event instanceof ItemRemovedEvent) {
				this.removeItem(event.item);
			} else if (event instanceof ItemUpdateEvent) {
				this.updateItem(event.item);
			} else if (event instanceof ItemFilterEvent) {
				this.filterItems(event.items,event.filterType);
			}
		}));
	}

	saveFilter(filterType){
		this.actualFilterType = filterType;
		localStorage.setItem("todoFilter",filterType);
	}

	filterItems(items,filterType){
		items.forEach((item) => {
			switch (filterType) {
				case 0:
					item._visible = true;
					break;
				case 1:
					item._visible = item._completed;
					break;
				case 2:
					item._visible = !item._completed;
					break;
			}

			const index = this._todoDescriptors.findIndex((descriptor) => descriptor.item === item);
			if (index === -1) throw new Error(`Unknown todo item: "${item.name}"`);
			const controller = this._todoDescriptors[index].controller;
			controller.update(item);
		});
	}

	setFilterButtons(){
		switch (this.actualFilterType) {
			case 0:
				this._elements.all.classList.add("active");
				this._elements.completed.classList.remove("active");
				this._elements.remaining.classList.remove("active");
				break;
			case 1:
				this._elements.all.classList.remove("active");
				this._elements.completed.classList.add("active");
				this._elements.remaining.classList.remove("active");
				break;
			case 2:
				this._elements.all.classList.remove("active");
				this._elements.completed.classList.remove("active");
				this._elements.remaining.classList.add("active");
				break;
		}
	}

	addItem(item) {
		const delegate = {
			remove: () => {
				this._todoStore.removeItem(item);
			},
			update: ()=> {
				this._todoStore.updateItem(item);
			}
		};
		const controller = new TodoListItem(item, delegate, this._templateProvider);
		this._todoDescriptors.push({
			item: item,
			controller: controller
		});
		this._elements.todoListItems.appendChild(controller.element);
		controller.attached();
	}

	removeItem(item) {
		const index = this._todoDescriptors.findIndex((descriptor) => descriptor.item === item);
		if (index === -1) throw new Error(`Unknown todo item: "${item.name}"`);
		const controller = this._todoDescriptors[index].controller;
		this._elements.todoListItems.removeChild(controller.element);
		controller.detached();
		this._todoDescriptors.splice(index, 1);
	}
	
	updateItem(item){
		const index = this._todoDescriptors.findIndex((descriptor) => descriptor.item === item);
		if (index === -1) throw new Error(`Unknown todo item: "${item.name}"`);
		const controller = this._todoDescriptors[index].controller;
		controller.update(item);
	}

	detached() {
		this._todoDescriptors.forEach((descriptor) => {
			this._elements.todoListItems.removeChild(descriptor.controller.element);
			descriptor.controller.detached();
		});

		this._todoDescriptors = [];

		this._disposables.forEach((f) => f.dispose());
		this._disposables = [];
	}
}

export class TodoListItem {
	constructor(item, delegate, templateProvider) {
		this._delegate = delegate;
		this._elements = templateProvider.create("todoListItem");
		this._templateProvider = templateProvider;
		this._disposables = [];
		//need to create TodoItem for this TodoListItem
		//new TodoItem(params)
		this._todoItem = new TodoItem(item, delegate, templateProvider);
		this._elements.itemContainer.appendChild(this._todoItem.element);
		this._todoItem.attached();
		this.update(item);
	}

	get element() {
		return this._elements.root;
	}

	update(item){
		this.setVisibility(item);
		this._todoItem.update(item);
	}

	setVisibility(item){
		if(item._visible){
			this._elements.root.classList.remove("hide");
		}else{
			this._elements.root.classList.add("hide");
		}
	}

	attached() {
		this._disposables.push(addEventListener(this._elements.removeButton, "click", () => {
			let confirm = new Confirm(this._templateProvider);
			let message = "Do You Want To Remove This Todo Element?";
			confirm.showMessage(message, () => {
				this._delegate.remove();
			});
		}));
		//Notify TodoItem.attached
	}
	
	detached() {
		//Notify TodoItem.detached
		this._disposables.forEach((f) => f.dispose());
		this._disposables = [];
	}
}

export class TodoItem{
	constructor(item, delegate, templateProvider) {
		this._item = item;
		this._delegate = delegate;
		this._elements = templateProvider.create("todoItem");
		this._disposables = [];
		this.setText();
		this.setChecked();
		this.setEditable();
	}
	
	get element() {
		return this._elements.root;
	}
	
	setEditable(){
		this._disposables.push(addEventListener(this._elements.itemText, "click", () => {
			this.generateEditor();
		}));
	}
	
	generateEditor(){
		let editor = document.createElement("input");
		editor.type = "text";
		editor.value = this._item._name;
		while(this._elements.itemText.firstChild){
			this._elements.itemText.removeChild(this._elements.itemText.firstChild);
		}
		this._elements.itemText.appendChild(editor);
		editor.focus();
		let disposable = addEventListener(this._elements.itemText, "keyup", (e) => {
			if(e.keyCode === 27){
				this.updateItem();
				disposable.dispose();
			}
			if(e.keyCode === 13){
				if(editor.value.length > 0){
					this._item._name = editor.value;
				}
				this.updateItem();
				disposable.dispose();
			}
		});
	}
	
	setText(newText){
		while(this._elements.itemText.firstChild){
			this._elements.itemText.removeChild(this._elements.itemText.firstChild);
		}
		let textNode = document.createTextNode(this._item._name);
		this._elements.itemText.appendChild(textNode);
	}

	setChecked(){
		this._elements.completedCheckbox.checked = this._item._completed;
	}
	
	updateItem(){
		this._item._completed = this._elements.completedCheckbox.checked;
		this._delegate.update();
	}
	
	update(item){
		this._item = item;
		this.setText();
		this._elements.completedCheckbox.checked = this._item._completed;
	}
	
	attached(){
		this._disposables.push(addEventListener(this._elements.completedCheckbox, "change", () => {
			this.updateItem();
		}));
	}

	detached() {
		this._disposables.forEach((f) => f.dispose());
		this._disposables = [];
	}
}