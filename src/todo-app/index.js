import { TodoStore } from "./model/todo-store.js";
import { TemplateProvider } from "./util/template-provider.js";
import { TodoList } from "./controller/todo-list.js";

export function run(element) {
	const todoStore = new TodoStore();
	const templateProvider = new TemplateProvider(element);

	const primaryContainer = element.querySelector(".primary-container");
	const secondaryContainer = element.querySelector(".secondary-container");

	const todoList = new TodoList(todoStore, templateProvider);
	primaryContainer.appendChild(todoList.element);
	todoList.attached();

	const todoList2 = new TodoList(todoStore, templateProvider);
	secondaryContainer.appendChild(todoList2.element);
	todoList2.attached();
}
