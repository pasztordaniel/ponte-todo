import { EventEmitter } from "../util/event-emitter.js";
import { Todo } from "../model/todo.js";

export class ItemAddedEvent {
	constructor(item) {
		this.item = item;
	}
}

export class ItemRemovedEvent {
	constructor(item) {
		this.item = item;
	}
}

export class ItemUpdateEvent {
	constructor(item) {
		this.item = item;
	}
}

export class ItemFilterEvent {
	constructor(items,filterType) {
		this.items = items;
		this.filterType = filterType;
	}
}

export class TodoStore {
	constructor() {
		this._todoItems = [];
		this._localStorageKey = "todoObj";
		this._savedObj = {};
		[this._changeEventListener, this._changeEventListenerTrigger] = EventEmitter.createEventEmitter();
		this.load();
	}
	
	get changeEventListener() {
		return this._changeEventListener;
	}
	
	get items() {
		return this._todoItems.slice(0);
	}
	
	filterBy(items,filterType){
		this._todoItems = items;
		items.forEach((item) => {
			this.updateItem(item);
		});
		this._changeEventListenerTrigger(new ItemFilterEvent(items,filterType));
		this.save();
	}

	addItem(item,save = true) {
		this._todoItems.push(item);
		this._changeEventListenerTrigger(new ItemAddedEvent(item));
		if(save){
			this.save();
		}
	}

	removeItem(item) {
		const index = this._todoItems.indexOf(item);
		if (index !== -1) {
			this._todoItems.splice(index, 1);
			this._changeEventListenerTrigger(new ItemRemovedEvent(item));
		}
		this.save();
	}
	
	updateItem(item) {
		const index = this._todoItems.indexOf(item);
		if (index !== -1) {
			this._changeEventListenerTrigger(new ItemUpdateEvent(item));
		}
		this.save();
	}

	save(){
		this._savedObj = {};
		let json;
		this.items.forEach((item, index) => {
			this._savedObj[index] = {};
			this._savedObj[index]._name = item._name;
			this._savedObj[index]._completed = item._completed;
			this._savedObj[index]._visible = item._visible;
		});
		json = JSON.stringify(this._savedObj);
		localStorage.setItem(this._localStorageKey,json);
	}

	load(){
		if(localStorage.getItem(this._localStorageKey) === null) return;
		this._savedObj = JSON.parse(localStorage.getItem(this._localStorageKey));
		for(var index in this._savedObj) { 
			this.addItem(new Todo({
				completed: this._savedObj[index]._completed,
				name: this._savedObj[index]._name,
				visible: this._savedObj[index]._visible
			}),false);
		}
	}
}
