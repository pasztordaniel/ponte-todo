
import { EventEmitter } from "../util/event-emitter.js";

export class NameChangedEvent {
	constructor(oldValue, newValue) {
		this.oldValue = oldValue;
		this.newValue = newValue;
	}
}

export class CompletedChangedEvent {
	constructor(oldValue, newValue) {
		this.oldValue = oldValue;
		this.newValue = newValue;
	}
}

export class VisibilityChangedEvent {
	constructor(oldValue, newValue) {
		this.oldValue = oldValue;
		this.newValue = newValue;
	}
}

export class Todo {
	constructor(config) {
		[this._changeEventListener, this._changeEventListenerTrigger] = EventEmitter.createEventEmitter();
		if (config.name) {
			this._name = config.name;
		} else {
			this._name = "unnamed";
		}
		if (config.completed) {
			this._completed = config.completed;
		} else {
			this._completed = false;
		}
		if (config.visible) {
			this._visible = config.visible;
		} else {
			this._visible = false;
		}
	}

	get changeEventListener() {
		return this._changeEventListener;
	}

	get name() {
		return this._name;
	}

	set name(newValue) {
		if (this._name === newValue) return;

		const oldValue = this._name;
		this._name = newValue;
		this._changeEventListenerTrigger(new NameChangedEvent(oldValue, newValue));
	}

	get completed() {
		return this._completed;
	}

	set completed(newValue) {
		if (this._completed === newValue) return;

		const oldValue = this._completed;
		this._completed = newValue;
		this._changeEventListenerTrigger(new CompletedChangedEvent(oldValue, newValue));
	}
	
	get visible(){
		return this._visible;
	}
	
	set visible(newValue){
		if (this._visible === newValue) return;
	
		const oldValue = this._visible;
		this._visible = newValue;
		this._changeEventListenerTrigger(new VisibilityChangedEvent(oldValue, newValue));
	}
}
