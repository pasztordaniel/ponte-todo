import { addEventListener } from "./event-listener.js";

export class Confirm{
	constructor(templateProvider){
		this._confirmContainer = document.querySelector(".confirm-container");
		this._templateProvider = templateProvider;
		this._elements = this._templateProvider.create("confirm");
		this._disposables = [];
	}

	get element() {
		return this._elements.root;
	}

	generate(message,callBack){
		this._elements.message.innerText = message;

		this._disposables.push(addEventListener(this._elements.modal, "click", () => {
			this.close();
		}));
		
		this._disposables.push(addEventListener(this._elements.closeX, "click", () => {
			this.close();
		}));
		
		this._disposables.push(addEventListener(this._elements.cancelButton, "click", () => {
			this.close();
		}));
		
		this._disposables.push(addEventListener(this._elements.removeButton, "click", () => {
			this.close();
			callBack();
		}));
		this._confirmContainer.appendChild(this.element);
	}

	showMessage(message,callBack){
		this.generate(message,callBack);
	}

	close(){
		this._confirmContainer.removeChild(this.element);
		this._disposables.forEach((f) => f.dispose());
		this._disposables = [];
	}
}