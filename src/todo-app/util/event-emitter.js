export class EventEmitter {
	constructor(bind) {
		this._handlers = [];
		bind.trigger = (...args) => {
			this._handlers.forEach((item) => {
				try {
					item.apply(this, args);
				} catch (e) {
					console.error(e);
				}
			})
		};
	}

	add(handler) {
		const indexOf = this._handlers.indexOf(handler);
		if (indexOf === -1) {
			this._handlers.push(handler);
		}

		let removeFunction = () => {
			this.remove(handler);
		};
		const dispose = () => {
			if (removeFunction) {
				removeFunction();
				removeFunction = undefined;
			}
		};
		return {
			dispose: dispose
		}
	}
	
	remove(handler) {
		const indexOf = this._handlers.indexOf(handler);
		if (indexOf !== -1) {
			this._handlers.splice(indexOf, 1);
		}
	}

	static createEventEmitter() {
		const bind = {
			trigger: null
		};
		const listener = new EventEmitter(bind);
		const trigger = bind.trigger;
		return [listener, trigger];
	}
}
