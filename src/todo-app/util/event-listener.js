export function addEventListener(element, type, handler) {
	element.addEventListener(type, handler);
	let remove = () => {
		element.removeEventListener(type, handler);
	}
	return {
		dispose: () => {
			if (remove) {
				remove();
				remove = undefined;
			}
		}
	}
}
