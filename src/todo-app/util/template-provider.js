export class TemplateProvider {
	constructor(hostElement) {
		this._templates = Array.from(hostElement.childNodes).filter((node) => {
			return node.nodeType === Node.ELEMENT_NODE && node.tagName.toLowerCase() === "template";
		});
	}

	getTemplateForId(id) {
		return this._templates.find((templateElement) => templateElement.getAttribute("data-template-id") === id);
	}

	create(id) {
		const template = this.getTemplateForId(id);
		if (!template) {
			throw new Error(`Unknown template "${id}"`);
		}
		const documentFragment = template.content.cloneNode(true);

		const result = {};
		documentFragment.querySelectorAll("[data-element-ref]").forEach((element) => {
			const ref = element.getAttribute("data-element-ref");
			result[ref] = element;
		});
		return result;
	}
}
